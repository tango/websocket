module gitea.com/tango/websocket

go 1.17

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e
	gitea.com/lunny/tango v0.6.5
	gitea.com/tango/debug v0.0.0-20190606021531-83f604669a11
	gitea.com/tango/renders v0.0.0-20191027160057-78fc56203eb4
	gitea.com/tango/session v0.0.0-20201110080243-87f6e468e457
	github.com/gorilla/websocket v1.4.2
)

require github.com/oxtoacart/bpool v0.0.0-20190530202638-03653db5a59c // indirect
